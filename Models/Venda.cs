using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        //Id Vendedor será recebido ao criar uma venda
        public int IdVendedor { get; set; }
        // Propriedade de navegação
        [ForeignKey("IdVendedor")]
        public Vendedor Vendedor { get; set; }       
         public EnumVendaStatus Status { get; set; }
        //Propriedade não mapeada para o EF, não vai para a tabela. Porém, é recebido pela API       
        // Id's dos produtos (Itens)
        [NotMapped]
        public List<int> Itens { get; set; }
       
        
        

    }
}