using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class VendaItem
    {
        public VendaItem(int idVenda, int idProduto)
        {
            IdVenda = idVenda;
            IdProduto = idProduto;
        }

// Os dois campos abaixo são chave primária. O data annotation comunica ao EF que os 
// campos não são gerados automaticamente

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdVenda { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdProduto { get; set; }
   // Propriedade de navegação
        [ForeignKey("IdVenda")]
        public Venda Venda { get; set; }
        [ForeignKey("IdProduto")]
        public Produto Produto { get; set; }

    }
}