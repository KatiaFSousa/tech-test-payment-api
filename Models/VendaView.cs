using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class VendaView
    {
        public VendaView(Venda venda, Vendedor vendedor, List<Produto> vendaProdutos)
        {
            this.Id = venda.Id;
            this.Data = venda.Data;
            this.Status = venda.Status;
            this.Vendedor = vendedor;
            this.Itens = vendaProdutos;
        }

         public int Id { get; set; }
        public DateTime Data { get; set; }
        public Vendedor Vendedor { get; set; }       
         public EnumVendaStatus Status { get; set; }
        public List<Produto> Itens { get; set; }
    }
}