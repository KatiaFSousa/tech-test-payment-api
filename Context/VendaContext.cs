
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class VendaContext : DbContext
    {
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<VendaItem> VendaItens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder){
            modelBuilder.Entity<VendaItem>()
            .HasKey(vi => new { vi.IdVenda, vi.IdProduto });
        }
// Essa chave composta define que não se pode ter o mesmo ID de venda para o mesmo ID 
// Produto, tornando a união de venda e produto como muitos para muitos.
    }
}