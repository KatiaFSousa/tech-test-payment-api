using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<VendaContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("Conexao")));


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();



// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(s => {
        //Mantem o apontamento para o swagger.json correto
        s.SwaggerEndpoint("/swagger/v1/swagger.json", "Api Venda"); 
        s.RoutePrefix = "api-docs"; //Muda a rota do swagger para api-docs
        });
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

//app.MapGet("/", async http =>  http.Response.Redirect("/api-docs"));

app.Run();
