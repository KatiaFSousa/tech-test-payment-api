using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Swashbuckle.AspNetCore.Annotations;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [SwaggerResponse(statusCode: 201, description: "Item Criado", Type = typeof(VendaView))]
        [SwaggerResponse(400, "Bad Request")]
        
        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            //Validação itens vazio
            if (!venda.Itens.Any())
                return BadRequest(new { Erro ="Falta informar produtos vendidos"});
            // Validação se o produto é existente na base.
            foreach (var item in venda.Itens)
            {
                var produto = _context.Produtos.Where(p => p.Id == item);
                if(!produto.Any())
                    return BadRequest(new { Erro = $"O produto com id {item} não foi encontrado"});
        }
            // Criação da venda
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            // Verificação de itens e adicionamento de ID Venda e Produto
            foreach(var item in venda.Itens){
                _context.VendaItens.Add(new VendaItem(venda.Id,item));
                _context.SaveChanges();
            }

            return CreatedAtAction(nameof(BuscarVenda), new{ id = venda.Id}, venda);
        }

        [HttpGet]
        public IActionResult BuscarVenda(int IdVenda)
        {
            //pega dados do banco
            var vendaBanco = _context.Vendas.Find(IdVenda);
            var vendedorBanco = _context.Vendedores.Find(vendaBanco.IdVendedor);
            var vendaItens = _context.VendaItens.Where(x=> x.IdVenda == IdVenda).ToList();
           
           // Cria uma lista de Produtos Vazia
            List<Produto> vendaProdutos = new List<Produto>();
    
           // Loop nos itens encontrados pelo IdVenda na tabela de VendaItens
            foreach(var item in vendaItens){
                //seleciona os produtos por Id na tabela de Produtos 
                var produto = _context.Produtos.Find(item.IdProduto);
                // Preenche a lista com o produto encontrado
                // da tabela de VendaItens através do IdVenda variável (vendaItens)
                vendaProdutos.Add(produto);
             
            }
            // Cria o objeto View de Venda usando o construtor da classe VendaView
            // retorna para a variavel vendaCompletaInfo que por sua vez
            // retorna o JSON para a API
            var vendaCompletaInfo = new VendaView(vendaBanco, vendedorBanco, vendaProdutos);

            return Ok(vendaCompletaInfo);
        }

        [HttpPut]
        
        public IActionResult AtualizarVenda(int id, EnumVendaStatus status)
        {
            //buscar a venda no banco
            var vendaBanco = _context.Vendas.Find(id);
            if (vendaBanco == null)
                return NotFound();
            //validar se a mudança de status é possível
            if (vendaBanco.Status == EnumVendaStatus.Pagamento 
                    && status == EnumVendaStatus.Aprovado 
                    || status == EnumVendaStatus.Cancelada
                    ){
                AtualizarVendaConfirmada(vendaBanco, status);
            }
            else if(vendaBanco.Status == EnumVendaStatus.Aprovado
                    && status == EnumVendaStatus.Enviado
                    || status == EnumVendaStatus.Cancelada
                    ){
                AtualizarVendaConfirmada(vendaBanco, status);

            }else if(vendaBanco.Status == EnumVendaStatus.Enviado
            && status == EnumVendaStatus.Entregue){
              AtualizarVendaConfirmada(vendaBanco, status);
            }else{
                //retorna badrequest
                return BadRequest(new { Erro = "Status não permitido" });
            }
          
            return Ok();
        }

        private void AtualizarVendaConfirmada(Venda vendaBanco, EnumVendaStatus status)
        {
            //atualiza o objeto que retornou do db
            vendaBanco.Status = status;
            // faz o update e salva mudanças
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            
        }
    }

}