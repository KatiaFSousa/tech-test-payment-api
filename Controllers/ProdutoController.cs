using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;

namespace tech_test_payment_api.Controllers
{
     [ApiController]
    [Route("[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly VendaContext _context;

        public ProdutoController(VendaContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult CriarProduto(ProdutoView produto)
        {
            var novoProduto = new Produto();
            novoProduto.Nome = produto.Nome;
            novoProduto.Preco = produto.Preco;

            _context.Produtos.Add(novoProduto);
            _context.SaveChanges();
            return Created("", novoProduto);
        }
    }
}